;;; Org-Mode Settings
(eval-after-load "org"
  '(require 'ox-gfm nil t))

(require 'org)
(require 'ox-gfm)
(require 'org-roam)
(setq org-directory (expand-file-name "~/OrgMode"))

(setq org-roam-directory "~/OrgMode/roam")
(add-hook 'after-init-hook 'org-roam-mode)

(let ((default-directory "~/OrgMode"))
  (setq org-notes-file (expand-file-name "notes.org"))
  (setq org-todo-file (expand-file-name "inbox.org"))
  (setq org-projects-file (expand-file-name "gtd.org"))
  (setq org-someday-file (expand-file-name "someday.org"))
  (setq org-journal-file (expand-file-name "journal.org"))
  )
(setq org-agenda-files '("~/OrgMode/inbox.org"
                         "~/OrgMode/gtd.org"))
;; (org-agenda-files '("~/OrgMode/inbox.org" "~/OrgMode""~/OrgMode/notes.org"))

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))))
(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold))))
(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING") ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))
(setq org-use-fast-todo-selection t)

;;; Capture templates
(setq org-capture-templates
      '(("t" "Todo" entry (file org-todo-file)
         "* TODO %?\n%U\n%a\n")
        ("j" "Journal" entry (file+olp+datetree org-journal-file)
         "* %?\nEntered on %U\n  %i\n  %a")
        ("n" "Note" entry (file org-notes-file)
         "* %?\nEntered on %U\n  %i\n  %a")
        )
      )

(setq org-refile-targets '(("~/OrgMode/gtd.org" :maxlevel . 3)
                           ("~/OrgMode/someday.org" :level . 1)))

;; (setq org-refile-targets (quote ((nil :maxlevel . 3)
;;                                  (org-agenda-files :maxlevel . 3))))
(setq org-refile-use-outline-path t)
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-allow-creating-parent-nodes (quote confirm))
(setq org-completion-use-ido t)

(defun org-toggle-iimage-in-org ()
  "display images in your org file"
  (interactive)
  (if (face-underline-p 'org-link)
      (set-face-underline-p 'org-link nil)
    (set-face-underline-p 'org-link t))
  (iimage-mode ‘toggle))

(define-key prelude-mode-map (kbd "C-c o") nil)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c o f") 'org-switchb)
(global-set-key (kbd "C-c c") 'org-capture)

(add-hook 'org-mode-hook
          (lambda ()
            (local-set-key "\M-n" 'outline-next-visible-heading)
            (local-set-key "\M-p" 'outline-previous-visible-heading)
            ;; table
            (local-set-key "\C-\M-w" 'org-table-copy-region)
            (local-set-key "\C-\M-y" 'org-table-paste-rectangle)
            (local-set-key "\C-\M-l" 'org-table-sort-lines)
            ;; display images
            (local-set-key "\M-I" 'org-toggle-iimage-in-org)
            ;; fix tab
            (local-set-key "\C-y" 'yank)))
