;;; epchris-settings --- Personal packages

;;; Commentary:
;;; This is my own personal hook into Prelude, to pull in custom packages, and
;;; set custom configuration

(setq ns-use-srgb-colorspace nil)

;;; Packages
(prelude-require-packages
 '(
   ag
   ample-theme
   comment-dwim-2
   company-lsp
   dash
   dash-at-point
   direnv
   docker
   docker-tramp
   dockerfile-mode
   expand-region
   flycheck
   flycheck-color-mode-line
   flycheck-pos-tip
   forge
   gist
   gradle-mode
   groovy-mode
   helm-ag
   helm-descbinds
   helm-projectile
   helm-tramp
   iedit
   iimage
   ivy
   js-comint
   karma
   molokai-theme
   multiple-cursors
   origami
   org
   persp-projectile
   perspective
   project-explorer
   projectile-rails
   rbenv
   robe
   rspec-mode
   scratch
   slim-mode
   smart-mode-line
   swiper
   tickscript-mode
   which-key
   yaml-mode
   yasnippet
   zeal-at-point
   ))

;; Enable DirEnv mode globally
(direnv-mode)

;; To support DASH integration, unset the prelude key bindings we want to use
(define-key prelude-mode-map (kbd "C-c d") nil)
(define-key prelude-mode-map (kbd "C-c e") nil)

;;
;; Platform-specific setups
;;
(cond
 ((eq 'darwin system-type)
  (progn
    (global-set-key (kbd "C-c d") 'dash-at-point)
    (global-set-key (kbd "C-c e") 'dash-at-point-with-docset)
    (setq mac-command-modifier 'meta)
    (setq mac-option-modifier 'super)
    (setq mouse-wheel-scroll-amount (quote (0.01)))
    )
  )
 ((eq 'gnu/linux system-type)
  (progn
    (global-set-key (kbd "C-c d") 'zeal-at-point)
    (global-set-key (kbd "C-c e") 'dash-at-point-with-docset)
    (setq ns-use-srgb-colorspace t)
    )
  )
 )

;;; UI Settings
(setq select-enable-clipboard t)
(setq-default truncate-lines t)
(setq system-uses-terminfo nil)
(prefer-coding-system 'utf-8)

;;; Smart Mode Line
(setq sml/no-confirm-load-theme t)
(sml/setup)
(sml/apply-theme 'automatic)

;;; Theme settings
(load-theme 'ample t t)
(load-theme 'ample-flat t t)
(enable-theme 'ample)

;;; Personal settings
(require 'compile)
(setq prelude-flyspell nil)

;; HTML/Javascript/Web mode things
(require 'karma)
(add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
      (let ((web-mode-enable-part-face nil))
        ad-do-it)
    ad-do-it))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  )

;; Display ANSI color codes in compilation buffers
(eval-after-load 'compile
  '(add-hook 'compilation-filter-hook
             (lambda () (ansi-color-process-output nil))))

(add-hook 'web-mode-hook  'my-web-mode-hook)

;; Use eslint from a node_modules directory
(defun my/use-eslint-from-node-modules ()
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))

(add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)

;; Project Explorer
(global-set-key [f8] 'project-explorer-toggle)

;; Line numbers and indentation
(global-linum-mode t)
(setq linum-format "%03d ")

;; Use spaces for tabs
(setq-default indent-tabs-mode nil)
(setq standard-indent 2)
(setq json-reformat:indent-width 2)

;; Kill without confirmation
(defun kill-current-buffer ()
  "Kill the current buffer, without confirmation."
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key "\C-xk" 'kill-current-buffer)

;; Keybinding for commenting regions
(global-set-key "\M-/" 'comment-dwim-2)

;; XML Formatting method
(defun xml-format ()
  (interactive)
  (save-excursion
    (shell-command-on-region (mark) (point) "xmllint --format -" (buffer-name) t)
    ))

;; Projectile settings
(persp-mode)
(require 'persp-projectile)
(setq projectile-switch-project-action 'projectile-dired)
(add-hook 'projectile-mode-hook 'projectile-rails-on)

;; JS/Coffee mode
(add-hook 'compilation-filter-hook
          (lambda () (ansi-color-process-output nil)))

;; YAML Mode
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
(add-hook 'yaml-mode-hook
          '(lambda ()
             (define-key yaml-mode-map (kbd "C-m") 'newline-and-indent)))

;; Multiple Cursors
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;;; Origami Settings
(require 'origami)
(define-key origami-mode-map (kbd "C-c C-o o") 'origami-open-node)
(define-key origami-mode-map (kbd "C-c C-o c") 'origami-close-node)
(define-key origami-mode-map (kbd "C-c C-o t") 'origami-recursively-toggle-node)
(define-key origami-mode-map (kbd "C-c C-o r") 'origami-reset)
(global-origami-mode)

;; Which key, for command help
(which-key-mode)

(advice-add 'risky-local-variable-p :override #'ignore)

;; Enable YASnippet globally
(yas-global-mode 1)

;; Company LSP support
;; (push 'company-lsp company-backends)
;; LSP-mode suggests this: https://emacs-lsp.github.io/lsp-mode/page/performance/#tuning
(setq read-process-output-max (* 1024 1024)) ;; 1mb

(provide 'epchris-settings)
