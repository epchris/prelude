;;;
;;; My settings for java development in Emacs
;;;

(with-eval-after-load 'lsp-mode
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration))

(add-hook 'java-mode-hook
          (lambda ()
            (lsp)
            (flycheck-mode +1)
            (google-set-c-style)
            (google-make-newline-indent)
            (setq c-basic-offset 2)))

(add-hook 'kotlin-mode-hook #'lsp)
(add-hook 'kotlin-mode-hook #'google-make-newline-indent)
;; (add-hook 'kotlin-mode-hook #'google-set-c-style)
