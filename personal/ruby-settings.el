;;
;; Ruby settings
;;

;; ROBE integration
(global-set-key (kbd "C-c r j") 'robe-jump)
(global-set-key (kbd "C-c r a") 'robe-ask)

(defun my-ruby-mode-hook ()
  (setq standard-indent 2)
  (linum-on)
  (subword-mode +1)
  (local-set-key (kbd "C-M-f") 'ruby-forward-sexp)
  (local-set-key (kbd "C-M-b") 'ruby-backward-sexp)
  (local-set-key (kbd "RET") 'newline-and-indent)
  (setq-local imenu-create-index-function 'ruby-imenu-create-index)
  (setq dash-at-point-docset "AllRuby"))

(add-hook 'ruby-mode-hook 'robe-mode)
(add-hook 'ruby-mode-hook 'my-ruby-mode-hook)
(add-hook 'ruby-mode-hook 'rspec-mode)
(add-hook 'after-init-hook 'inf-ruby-switch-setup)
(eval-after-load 'rspec-mode
  '(rspec-install-snippets))
(add-hook 'rspec-before-verification-hook 'direnv-update-environment)
(add-hook 'robe-mode-hook 'ansi-color-for-comint-mode-on)
(add-hook 'inf-ruby-mode-hook 'ansi-color-for-comint-mode-on)
(setq ruby-deep-indent-paren nil)
(add-hook 'ruby-mode-hook #'lsp)
